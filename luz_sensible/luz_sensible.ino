#define LED 9     //El LED esta conectado en el pin 9 
#define LDR A0     //El LDR esta conectador en el pin A0
#define POT A2 // Potenciometro en pin A2
#define BUT 8
  
int sens_val = 0;       
int lim_val = 0; 
int lim = 988;
__attribute__((section(".noinit"))) int state;

void setup() {     
  Serial.begin(9600);    
  pinMode(LED,OUTPUT);
  pinMode(LDR,INPUT);
  pinMode(POT,INPUT);

  state = (state == 0) ? 1 : 0;
}

void loop ()  
{
  sens_val = analogRead(LDR);
  lim_val = analogRead(POT); 

  Serial.println(lim_val);

  switch(state) {
  case 0:
    if (sens_val <= lim_val)   
    {
      digitalWrite (LED, LOW);  //El led se apaga
    }
    if (sens_val > lim_val)  
    {
      digitalWrite (LED, HIGH);  //El led se prende
    }
    break;
  default:
    if (sens_val <= lim)   
    {
      digitalWrite (LED, LOW);  //El led se apaga
    }
    if (sens_val > lim)  
    {
      digitalWrite (LED, HIGH);  //El led se prende
    }
    break;
  }
}
