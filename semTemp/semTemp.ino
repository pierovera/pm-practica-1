int ban = 0;
int cont = 0;

int RS1 = 13;
int YS1 = 12;
int GS1 = 11;
int RS2 = 10;
int YS2 = 9;
int GS2 = 8;

void setup() {
  pinMode(GS2, OUTPUT);
  pinMode(YS2, OUTPUT);
  pinMode(RS2, OUTPUT);
  pinMode(GS1, OUTPUT);
  pinMode(YS1, OUTPUT);
  pinMode(RS1, OUTPUT);
  
  Serial.begin(9600);

  digitalWrite(GS1, HIGH);
  digitalWrite(RS2, HIGH);
  digitalWrite(RS1, LOW);
  digitalWrite(YS1, LOW);
  digitalWrite(YS2, LOW);
  digitalWrite(GS2, LOW);
  
  DDRB = DDRB | B11111111; 
  DDRD = DDRD | B11111111; 
  cli  ();
  TCCR1B = 0;
  TCCR1A = 0;
  TCCR1B |= (1 << CS12);
  TCNT1 = 0;
  OCR1A = 15624; // = (16*10^6) / (1*1024) - 1 (<65536)
  TIMSK1 |= (1 << TOIE1);
  sei ();
}
ISR(TIMER1_OVF_vect){
  cont = cont + 1;
  Serial.print(cont);
}

void loop() {
  switch(cont) {
    case 6:
      digitalWrite(GS1, LOW);
      digitalWrite(YS1, HIGH);
      break;
    case 7:
      digitalWrite(RS1, HIGH);
      digitalWrite(YS1, LOW);
      digitalWrite(GS2, HIGH);
      digitalWrite(RS2, LOW);
      break;
    case 12:
      digitalWrite(YS2, HIGH);
      digitalWrite(GS2, LOW);
      break;
    case 13:
      digitalWrite(RS2, HIGH);
      digitalWrite(YS2, LOW);
      digitalWrite(GS1, HIGH);
      digitalWrite(RS1, LOW);
      break;
    case 14:
      cont = 1;
      break;
  }
}
