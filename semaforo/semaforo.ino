#define CLOCK 16000000

uint16_t delayN;
void delayASM(uint16_t ms);
void aux(uint16_t ms);

void setup(){
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  delayN = CLOCK/4000;
  Serial.begin(9600);    
}

void loop(){
  digitalWrite(8, HIGH);
  digitalWrite(13,HIGH);
  delayASM(6000);
  digitalWrite(8,LOW);
  digitalWrite(9,HIGH);
  delayASM(1000);
  digitalWrite(9,LOW);
  digitalWrite(10,HIGH);
  digitalWrite(11,HIGH);
  digitalWrite(13,LOW);
  delayASM(5000);
  digitalWrite(11,LOW);
  digitalWrite(12,HIGH);
  delayASM(1000);
  digitalWrite(8, HIGH);
  digitalWrite(10,LOW);
  digitalWrite(12,LOW);
  digitalWrite(13,HIGH);
  delayASM(1000);
}

void delayASM(uint16_t ms){
  uint16_t i;
  asm volatile(
    "\n"
    "L_dl1%=:" "\n\t"
    "mov %A0, %A2" "\n\t"
    "mov %B0, %B2" "\n"
    "L_dl2%=:" "\n\t"
    "sbiw %A0, 1" "\n\t"
    "brne L_dl2%=" "\n\t"
    "dec %A1" "\n\t"
    "brne L_dl1%=" "\n\t"
    "dec %A1" "\n\t"
    "dec %B1" "\n\t"
    "brne L_dl1%=" "\n\t"
    : "=&w" (i) 
    : "r" (ms), "r" (delayN)
  );
}
